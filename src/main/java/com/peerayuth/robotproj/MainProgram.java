/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.robotproj;

import java.util.Scanner;

/**
 *
 * @author Ow
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner wn = new Scanner(System.in);
        TableMap map = new TableMap(10, 10);
        Robot robot = new Robot(2, 2, 'x', map);
        Bomb bomb = new Bomb(5, 5);
        map.setRobot(robot);
        map.setBomb(bomb);
        while(true) {
            map.showMap();
            // W,a | N,w | E,d | S,s | q : quit
            char direction = inputDirection(wn);
            if (direction == 'q'){
                printBye();
                break;
            }
            robot.walk(direction);
        }
        
    }

    private static void printBye() {
        System.out.println("Bye");
    }

    private static char inputDirection(Scanner wn) {
        String str = wn.next();
        return str.charAt(0);
    }
}
